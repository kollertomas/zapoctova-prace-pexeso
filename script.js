const temata = {
    zvirata: [
        'images/animals/1.jpg', 'images/animals/2.jpg', 'images/animals/3.jpg', 
        'images/animals/4.jpg', 'images/animals/5.jpg', 'images/animals/6.jpg',
        'images/animals/7.jpg', 'images/animals/8.jpg'
    ],
    ovoce: [
        'images/fruits/1.jpg', 'images/fruits/2.jpg', 'images/fruits/3.jpg', 
        'images/fruits/4.jpg', 'images/fruits/5.jpg', 'images/fruits/6.jpg',
        'images/fruits/7.jpg', 'images/fruits/8.jpg'
    ]
};

let dataHry = {
    obtiznost: 4,
    rezim: 'pc',
    chytrost: 'hloupy',
    tema: 'zvirata',
    skoreHrac1: 0,
    skoreHrac2: 0,
    aktualniHrac: 1,
    deska: [],
    pcPamet: {},
    paryHrac1: 0,
    paryHrac2: 0
};

function zacniHru() {
    dataHry.obtiznost = parseInt(document.getElementById('obtiznost').value);
    dataHry.rezim = document.getElementById('rezim').value;
    dataHry.chytrost = document.getElementById('chytrost').value;
    dataHry.tema = document.getElementById('tema').value;
    dataHry.skoreHrac1 = 0;
    dataHry.skoreHrac2 = 0;
    dataHry.paryHrac1 = 0;
    dataHry.paryHrac2 = 0;
    dataHry.aktualniHrac = 1;
    dataHry.pcPamet = {};
    document.getElementById('statistikyHrac1').innerText = 'Hráč 1: 0';
    document.getElementById('statistikyHrac2').innerText = 'Hráč 2: 0';

    nastavDesku();
}

function nastavDesku() {
    const deska = document.getElementById('herniDeska');
    deska.innerHTML = '';
    deska.setAttribute('data-velikost', dataHry.obtiznost);

    const temaArray = temata[dataHry.tema];
    let hodnotyKaret = temaArray.slice(0, dataHry.obtiznost / 2);
    hodnotyKaret = [...hodnotyKaret, ...hodnotyKaret];
    hodnotyKaret = hodnotyKaret.sort(() => Math.random() - 0.5);

    dataHry.deska = hodnotyKaret.map((hodnota, index) => ({
        id: index,
        hodnota: hodnota,
        otocena: false,
        sparovana: false
    }));

    dataHry.deska.forEach(karta => {
        const kartaElement = document.createElement('div');
        kartaElement.classList.add('karta');
        kartaElement.dataset.id = karta.id;
        kartaElement.addEventListener('click', () => otocKartu(karta.id));
        deska.appendChild(kartaElement);
    });
}

function otocKartu(id) {
    if (dataHry.aktualniHrac !== 1 && dataHry.rezim === 'pc') return; // Pouze hráč může otočit kartu v režimu proti PC

    const karta = dataHry.deska.find(karta => karta.id === id);
    if (!karta || karta.otocena || karta.sparovana) return;

    karta.otocena = true;
    const kartaElement = document.querySelector(`.karta[data-id="${id}"]`);
    kartaElement.classList.add('otocena');
    kartaElement.innerHTML = `<img src="${karta.hodnota}" alt="Karta">`;

    const otoceneKarty = dataHry.deska.filter(karta => karta.otocena && !karta.sparovana);
    if (otoceneKarty.length === 2) {
        setTimeout(() => {
            if (otoceneKarty[0].hodnota === otoceneKarty[1].hodnota) {
                otoceneKarty[0].sparovana = true;
                otoceneKarty[1].sparovana = true;
                aktualizujSkore();
                if (dataHry.rezim === 'pc') {
                    setTimeout(tahPc, 1000); // Počítač může pokračovat, pokud je režim "pc"
                }
            } else {
                otoceneKarty[0].otocena = false;
                otoceneKarty[1].otocena = false;
                document.querySelector(`.karta[data-id="${otoceneKarty[0].id}"]`).classList.remove('otocena');
                document.querySelector(`.karta[data-id="${otoceneKarty[0].id}"]`).innerHTML = '';
                document.querySelector(`.karta[data-id="${otoceneKarty[1].id}"]`).classList.remove('otocena');
                document.querySelector(`.karta[data-id="${otoceneKarty[1].id}"]`).innerHTML = '';
                if (dataHry.rezim === 'pc') {
                    dataHry.aktualniHrac = 2; 
                    setTimeout(tahPc, 1000);
                } else {
                    dataHry.aktualniHrac = dataHry.aktualniHrac === 1 ? 2 : 1;
                }
            }
        }, 1000);
    }
}

function aktualizujSkore() {
    if (dataHry.aktualniHrac === 1) {
        dataHry.skoreHrac1++;
        dataHry.paryHrac1++;
        document.getElementById('statistikyHrac1').innerText = `Hráč 1: ${dataHry.skoreHrac1}`;
    } else {
        dataHry.skoreHrac2++;
        dataHry.paryHrac2++;
        document.getElementById('statistikyHrac2').innerText = `Hráč 2: ${dataHry.skoreHrac2}`;
    }
}

function tahPc() {
    if (dataHry.rezim !== 'pc' || dataHry.aktualniHrac !== 2) return;

    let dostupneKarty = dataHry.deska.filter(karta => !karta.otocena && !karta.sparovana);
    let karta1, karta2;

    if (dataHry.chytrost !== 'hloupy' && Math.random() < 0.75 && Object.keys(dataHry.pcPamet).length > 0) {
        const znamePary = Object.entries(dataHry.pcPamet).filter(([klic, hodnota]) => hodnota.pocet === 2);
        if (znamePary.length > 0) {
            karta1 = dataHry.deska.find(karta => karta.id === znamePary[0][1].karty[0]);
            karta2 = dataHry.deska.find(karta => karta.id === znamePary[0][1].karty[1]);
        }
    }

    if (!karta1 || !karta2) {
        karta1 = dostupneKarty[Math.floor(Math.random() * dostupneKarty.length)];
        dostupneKarty = dataHry.deska.filter(karta => !karta.otocena && !karta.sparovana && karta.id !== karta1.id);
        karta2 = dostupneKarty[Math.floor(Math.random() * dostupneKarty.length)];
    }

    otocKartuPc(karta1.id);
    setTimeout(() => {
        otocKartuPc(karta2.id);

        if (dataHry.chytrost !== 'hloupy') {
            if (!dataHry.pcPamet[karta1.hodnota]) {
                dataHry.pcPamet[karta1.hodnota] = { pocet: 0, karty: [] };
            }
            dataHry.pcPamet[karta1.hodnota].pocet++;
            dataHry.pcPamet[karta1.hodnota].karty.push(karta1.id);

            if (!dataHry.pcPamet[karta2.hodnota]) {
                dataHry.pcPamet[karta2.hodnota] = { pocet: 0, karty: [] };
            }
            dataHry.pcPamet[karta2.hodnota].pocet++;
            dataHry.pcPamet[karta2.hodnota].karty.push(karta2.id);
        }

        if (karta1.hodnota === karta2.hodnota) {
            karta1.sparovana = true;
            karta2.sparovana = true;
            aktualizujSkore();
            setTimeout(tahPc, 1000); 
        } else {
            setTimeout(() => {
                karta1.otocena = false;
                karta2.otocena = false;
                document.querySelector(`.karta[data-id="${karta1.id}"]`).classList.remove('otocena');
                document.querySelector(`.karta[data-id="${karta1.id}"]`).innerHTML = '';
                document.querySelector(`.karta[data-id="${karta2.id}"]`).classList.remove('otocena');
                document.querySelector(`.karta[data-id="${karta2.id}"]`).innerHTML = '';
                dataHry.aktualniHrac = 1; 
            }, 1000);
        }
    }, 1000);
}

function otocKartuPc(id) {
    const karta = dataHry.deska.find(karta => karta.id === id);
    if (!karta || karta.otocena || karta.sparovana) return;

    karta.otocena = true;
    const kartaElement = document.querySelector(`.karta[data-id="${id}"]`);
    kartaElement.classList.add('otocena');
    kartaElement.innerHTML = `<img src="${karta.hodnota}" alt="Karta">`;
}

document.getElementById('zacniHru').addEventListener('click', zacniHru);
